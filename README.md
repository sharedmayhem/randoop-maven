#What?
This is a slightly modified version of the original Randoop maven plugin that can be found [here](https://github.com/zaplatynski/randoop-maven-plugin)

#Why
- Upgraded to Randoop v4.0.4 
- Allowing a generic way to pass options to Randoop 
- Consume the error and output stream of Randoop and log it to Maven 